package pacotelecd;

/**
 *
 * @author Deive
 */
class No {
    
    private Bloco objeto;
    private No referencia;

    // Construtor
    public No(int bloco) {
        Bloco meuBloco = new Bloco(bloco);
        this.setObjeto(meuBloco);
    }
    
    // Registra referência para o próximo nó
    public void setProximo(No referenciaProximoNo) {
        this.referencia = referenciaProximoNo;
    } 

    // Pegando a referência para o próximo objeto
    public No getProximo() {
        return this.referencia;
    }

    // Setando objeto Bloco
    public void setObjeto(Bloco meuBloco){
        this.objeto = meuBloco;
    }
    
    // Get objeto bloco.
    public Bloco getObjeto() {
        return this.objeto;
    }
    
}
