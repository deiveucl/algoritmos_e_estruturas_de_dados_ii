package pacotelecd;

/**
 *
 * @author Deive
 */
class Bloco {
    
    private int bloco;
    
    // Construtor
    public Bloco(int bloco) {
        this.bloco = bloco;
    }
    
    // Pegar o bloco
    public int getBloco() {
        return this.bloco;
    }

    // Setar o bloco
    public void setBloco(int bloco) {
        this.bloco = bloco;
    }
           
}
