package pacotelecd;

import javax.swing.JOptionPane;

/**
 *
 * @author Deive
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        // Atributo para manipular o descritor e as operações da lista.
        /*
        EstruturasDeDados listaEncadeadaComBlocos = new EstruturasDeDados();
        
        listaEncadeadaComBlocos.inserirDados(10, 0);
        listaEncadeadaComBlocos.inserirDados(11, 1);
        listaEncadeadaComBlocos.inserirDados(12, 2);
        listaEncadeadaComBlocos.removerDados(0);
        listaEncadeadaComBlocos.inserirDados(10, 0);
        listaEncadeadaComBlocos.inserirDados(13, 3);
        listaEncadeadaComBlocos.removerDados(2);
        listaEncadeadaComBlocos.inserirDados(12, 2);
        
        System.out.println(listaEncadeadaComBlocos.imprimirLista());
        */
        
        TBlocos tabelaBlocos = new TBlocos(5);
        
        System.out.println(tabelaBlocos.mostrarBlocos());
        
        //System.out.println(tabelaBlocos.moverPraCima(10));
        
    }
    
}
