/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pacotelecd;

/**
 *
 * @author Deive
 */
public class TBlocos {
    
    EstruturasDeDados[] meusBlocos;
    
    public TBlocos(int qtdBlocos) {
        this.meusBlocos = new EstruturasDeDados[qtdBlocos];
        
        for (int i = 0; i < meusBlocos.length; i++) {
            this.meusBlocos[i] = new EstruturasDeDados();
            this.meusBlocos[i].inserirDados(i, 0);
        }
    }
    
    public String mostrarBlocos() {
        String msg = "";
        
        for (int i = 0; i < this.meusBlocos.length; i++) {
            msg += i + ": " + meusBlocos[i].imprimirLista() + "\n";
        }
        
        return msg;
    }
    
    public boolean moverPraCima(int procuraBloco) {
        int posicaoVetor;
        Bloco blocoEncontrado;
        
        for (int i = 0; i < meusBlocos.length; i++) {
            posicaoVetor = i;
            blocoEncontrado = this.meusBlocos[i].retornarBloco(procuraBloco);
            
            return true;
        }
        
        return false;
    }
    
}
