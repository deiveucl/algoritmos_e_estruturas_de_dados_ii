 package pacotelecd;

/**
 *
 * @author Deive
 */
class EstruturasDeDados {
    
    // Descritor da lista simplesmente encadeada
    private No inicioDaLista;
    private int quantidadeDeNos;
    private No finalDaLista;
    
    /*-----------------------------------
             CONSTRUTOR DA CLASSE
      -----------------------------------*/
    
    public EstruturasDeDados(){ }
    
    /*-----------------------------------
               MÉTODOS DA CLASSE
      -----------------------------------*/
    
    // Método para alterar o atributo quantidadeDeNos.
    // _qdn quantidade de nós
    public void setQuantidadeDeNos(int _qdn) {
        this.quantidadeDeNos = _qdn;
    }
    
    // Método que retorna a quantidade de nós da lista.
    public int getQuantidadeDeNos() {
        return this.quantidadeDeNos;
    }
    
    // Método para alterar o atributo inicio da lista.
    // _iL = Início da lista
    public void setInicioDaLista(No _iL) {
        this.inicioDaLista = _iL;
    }
    
    // Método que retorna a referência para o primeiro nó da lista.
    public No getInicioDaLista() {
        return this.inicioDaLista;
    }
    
    // Método para alterar o atributo final da lista.
    // _fL = Início da lista
    public void setFinalDaLista(No _fL) {
        this.finalDaLista = _fL;
    }
    
    // Método que retorna a referência para o último nó da lista.
    public No getFinalDaLista() {
        return this.finalDaLista;
    }

    // Método privado para criar a lista encadeada.
    public void inicializarLE() {
        this.setInicioDaLista(null);
        this.setQuantidadeDeNos(0);
        this.setFinalDaLista(null);
    }
    
    // Método que verifica se a lista está vazia.
    public boolean isEmpty() {
        // Se a quantidade de nós da lista tiver o valor 0
        // quer dizer que a lista está vazia.
        return this.getQuantidadeDeNos() == 0;
    }
    
    // Método para limpar a lista encadeada.
    public void limparLE() {
        // Se a lista não estiver vazia, limpa ela, ou seja, incializa!
        if (!this.isEmpty()) {
            this.inicializarLE();
        }
    }
    
    // Método para inserir dados na lista encadeada.
    public boolean inserirDados(int bloco, int p){
        // Se posição negativa ou maior que a quantidade de nós
        if ((p < 0) || (p > this.getQuantidadeDeNos())){
            return false; // Quer dizer que não será possível inserir dados.
        } else { // Caso contrário
            // Cria-se uma instância da classe 'No'.
            No novoNo = new No(bloco);
            
            // Caso 1: Lista vazia
            if (this.isEmpty()){
                // Registra null como próxima referência.
                novoNo.setProximo(null);
                
                // Diz para  descritor quem é o 1º e último nó da lista.
                this.setInicioDaLista(novoNo);
                this.setFinalDaLista(novoNo);
            } else {
                // Caso 2 -> Posição = 0
                if (p == 0) {
                    // O endereço do 1º nó da lista é copiado para
                    // o campo próximo do novoNo.
                    novoNo.setProximo(this.getInicioDaLista());
                    
                    // Registra o novoNo como 1º nó da lista.
                    this.setInicioDaLista(novoNo);
                } else {
                    // Caso 3 = posicao será após o último nó da lista.
                    if (p == this.getQuantidadeDeNos()) {
                        // Registra null como próxima referência.
                        novoNo.setProximo(null);
                        
                        // O último nó da lista a partir deste momento
                        // tem o endereço do novoNo.
                        this.getFinalDaLista().setProximo(novoNo);
                        
                        // Registra o novoNo como último nó da lista.
                        this.setFinalDaLista(novoNo);
                    } else {
                        // Caso 4 -> Inserir quando p > 0 e p < qN
                        
                        // Atributo responsável por percorrer a lista.
                        // Começa apontando para o 1º nó da lista.
                        No ponteiroAuxiliar = this.getInicioDaLista();
                        
                        // Necessário percorrer da 1ª posição até uma posição
                        // antes da que realmente quer inserir.
                        // Para avançar para o próximo nó, necessário acessar
                        // o campo próximo.
                        for (int i = 0; i < (p-1); i++)
                            ponteiroAuxiliar = ponteiroAuxiliar.getProximo();
                        
                        // Registra o endereço do nó que está na posição 'p'
                        // no campo próximo do novoNo.
                        novoNo.setProximo(ponteiroAuxiliar.getProximo());
                        
                        // O nó anterior ao novoNo registra o endereço do mesmo.
                        ponteiroAuxiliar.setProximo(novoNo);
                    }
                }
            }
            
            // Atualiza a quantidade de nós da lista.
            this.setQuantidadeDeNos(this.getQuantidadeDeNos()+1);
            return true;
        }
        
    }
    
    // Método para remover um nó da lista encadeada.
    public boolean removerDados(int p){
        // Se a lista estiver vazia ou
        // Se posição negativa ou
        // Se posição maior ou igual a quantidade de nós
        if ((this.isEmpty()) || (p < 0) || (p >= this.getQuantidadeDeNos())){
            return false; // Não é possível remover nós da lista.
        } else {
            // Caso 1 - Se tiver apenas 1 elemento na lista
            if (this.getQuantidadeDeNos() == 1){
                // Inicializa os valores do descritor da lista.
                this.inicializarLE();
            } else {
                // Atributo responsável por percorrer a lista encadeada.
                // Ela recebe uma cópia do endereço do nó que está na
                // 1ª posição da lista.
                No referenciaAtual = this.getInicioDaLista();

                // Caso 2 - O nó que está na posição zero será excluído.
                if (p == 0){
                    // Registro o início da lista como sendo o 2º nó da lista.
                    this.setInicioDaLista(referenciaAtual.getProximo());
                    
                    // O nó a ser excluído, não terá mais acesso a outro nó.
                    referenciaAtual.setProximo(null);
                } else {
                    // Sentinela usado para percorrer até a posição desejada.
                    int contador = 0;
                    
                    // Enquanto não chegar na posição anterior a que se
                    // quer remover o nó, vai percorrendo a lista.
                    while(contador < (p - 1)){
                        referenciaAtual = referenciaAtual.getProximo();
                        contador++;
                    }
                    
                    // Quando chegar na posição desejada, verificar se:
                    // - Caso 3 ou 4.
                    
                    // Caso 3 - Remover o nó que está
                    // na última posição da lista;
                    if(p == (this.getQuantidadeDeNos()-1)){
                        // Diz que o nó atual não terá mais acesso ao endereço
                        // do último nó da lista.
                        referenciaAtual.setProximo(null);
                        
                        // O último nó da lista passa a ser
                        // o nó anterior ao que foi removido.
                        this.setFinalDaLista(referenciaAtual);
                        
                    } else {
                        // Caso 4 - Remoção de um nó que esteja em uma
                        // posição p > 0 e p < quantidade de nos - 1
                        
                        // Copia o endereço do nó a ser removido.
                        No proximaReferencia = referenciaAtual.getProximo();
                        
                        // Registra o endereço do nó posterior ao nó a
                        // ser removido como endereço do nó atual (nó anterior
                        // ao que será removido.
                        referenciaAtual.setProximo(proximaReferencia.getProximo());
                        
                        // O nó a ser removido não tem mais acesso ao 
                        // endereço do próximo nó.
                        proximaReferencia.setProximo(null);
                    }
                }
            }
            
            // Atualiza a quantidade de nós da lista.
            this.setQuantidadeDeNos(this.getQuantidadeDeNos()-1);
            return true;
        }
        
    }
    
    /*
     Método para alterar dados de um nó da lista.
       
       A forma de alteração poderá ser:
       1 - Acessando uma determinada posição, ou
       2 - Pesquisando pela chave (código).
    
       Sugere-se que o método pesquisarElemento() seja utilizado.
    
    public boolean alterarDados(int codigoAntigo, int novoCodigo, String novoNome){
        
        
        
    }
    */
    
    /*
    // Método para pesquisar um determinado registro através
    // de uma chave (na aplicação, essa chave será o código).
    public int pesquisarElemento(int codigo){
        
        
        
    }
    */

    /*
    // Método para resgatar um elemento de uma determianda posição 'p'.
    public String acessarDados(int p){
    
    
    
    }
    */
    
    // Método para imprimir a lista encadeada (relatório dos dados dos usuáios)
    public String imprimirLista() {
        // Atributo responsável por armazenar os dados presentes na lista.
        String relatorio = "";
        
        // Se a lista não estiver vazia, que dizer que existem nós na lista.
        if (!this.isEmpty()) {
            // Atributo para percorrer a lista encadeada.
            No auxiliar = this.getInicioDaLista();
            
            // Enquanto não chegar no final da lista.
            while (auxiliar != null) {
                // Vai resgatando os valores do nó (dados do usuário) e já vai formatando a string de retorno.
                relatorio = relatorio + "" + auxiliar.getObjeto().getBloco() + " ";
                
                // Avança para  o próximo nó.
                auxiliar = auxiliar.getProximo();
            }
            
        }
        
        // Retorna a string vazia ou com dados.
        return relatorio;
    }
    
    // Retorna objeto encontrado
    public Bloco retornarBloco(int bloco) {
        No auxiliar = this.getInicioDaLista();
        
        while (auxiliar != null) {
            if (bloco == auxiliar.getObjeto().getBloco()) {
                auxiliar = auxiliar.getProximo();
                break;
            }
        }
        
        return auxiliar.getObjeto();
    }
    
}
