package pacotelcd;

/**
 *
 * @author Jefferson
 */
class EstruturasDeDados {
    
    /*-----------------------------------
              ATRIBUTOS DA CLASSE
      -----------------------------------*/
    
    private Usuario[] ListaDeUsuarios; // Atributo lista do tipo Usuário
    
    // Descritores da lista contígua
    private int FimDaLista;
    private int TamanhoDaLista;
    
    /*-----------------------------------
             CONSTRUTOR DA CLASSE
      -----------------------------------*/
    
    // Construtor da classe
    // tl = Tamanho da lista
    public EstruturasDeDados(int _tl) {
        this.criarLista(_tl);
    }
    
    /*-----------------------------------
               MÉTODOS DA CLASSE
      -----------------------------------*/
    
    // Método que registra a instância da lista de usuários.
    public Usuario[] getLista(){
        return this.ListaDeUsuarios;
    }
    
    // Método que seta a lista de usuários.
    public void setLista(Usuario[] _lu){
        this.ListaDeUsuarios = _lu;
    }
    
    // Método que retorna a posição do último elemento da lista.
    public int getFimDaLista() {
        return FimDaLista;
    }

    // Método para alterar o atributo FimDaLista.
    public void setFimDaLista(int _fl) {
        this.FimDaLista = _fl;
    }
    
    // Método que retorna o tamanho da lista.
    public int getTamanhoDaLista() {
        return TamanhoDaLista;
    }
    
    // Método para alterar o atributo TamanhoDaLista.
    public void setTamanhoDaLista(int _tl) {
        this.TamanhoDaLista = _tl;
    }

    /*-----------------------------------
            OPERAÇÕES DO TAD LISTA CD
      -----------------------------------*/
    
    // Método para criair a lista contígua.
    private void criarLista(int _tl){
        
        // Registrando a instância da lista em tempo de execução
        this.setLista(new Usuario[_tl]);
        
        this.setFimDaLista(-1);
        this.setTamanhoDaLista(_tl);
        
    }
    
    // Método para limpar a lista contígua.
    public void limparLista(){
        this.setFimDaLista(-1);
    }
    
    // Método que verifica se a lista está vazia.
    public boolean isEmpty(){
        
        // Se o fim da lista tiver o valor -1
        // quer dizer que a lista está vazia.
        return this.getFimDaLista() == -1;
        
    }
    
    // Método que verifica se a lista está cheia.
    public boolean isFull(){
        
        // Se o fim da lista for exatamente o tamanho da lista
        // quer dizer que a lista está cheia.
        return this.getFimDaLista() == this.getTamanhoDaLista()-1;
        
    }
    
    // Método para inserir dados na lista contígua.
    public boolean inserirDados(int p, int c, String n){
        
        // Se a Lista estiver cheia ou
        // posição negativa ou
        // posição maior que o fim da lista (deixando buraco)
        if((this.isFull()) || (p < 0) || (p > this.getFimDaLista()+1)) {
            
            return false; // Quer dizer que não será possível inserir dados
        
        }else {
            
            // Cria-se uma nova instância da classe 'Usuario' e já insere os dados
            Usuario dados = new Usuario(c,n);
            
            // Se o usuário deseja inserir na próxima posíção válida
            if (p == (this.getFimDaLista()+1)){
                
                this.setFimDaLista(this.getFimDaLista()+1); // Define a posição
                this.getLista()[p] = dados; // Insere o objeto inteiro na posição 'p'
            
            }else {
                
                // Caso contrário, percorre do fim da lista até a posição 'p'
                // com o objetivo de abrir um espaço na lista para inserir o objeto
                // sem perder os outros objetos.
                for (int i = this.getFimDaLista(); i >= p; i--)
                    this.getLista()[i+1] = this.getLista()[i];
                
                this.getLista()[p] = dados; // Insere o objeto
                this.setFimDaLista(this.getFimDaLista()+1); // Atualiza o fim da lista
                
            }

            return true;
            
        }
        
    }
    
    // Método para remover dados da lista contígua.
    public boolean removerDados(int p){
        
        // Se a Lista estiver vazia ou
        // posição negativa ou
        // posição maior que o fim da lista
        if ((this.isEmpty()) || (p < 0) || (p > this.getTamanhoDaLista())){
        
            return false; // Retorna dizendo que não foi possível remover
        
        }else{
            
            // Se a posição for exatamente o fim da lista, basta
            // decrementar em 1 unidade o atributo Fim da Lista
            if (p == this.getFimDaLista()){
                
                this.setFimDaLista(this.getFimDaLista()-1);
            
            }else{
                
                // Caso contrário, percorre da posição 'p' até o penúltimo elemento da lista
                // Pega o próximo elemento para a posição anterior
                for(int i = p; i < this.getFimDaLista() ; i++)
                    this.getLista()[i] = this.getLista()[i+1];
                
                this.setFimDaLista(this.getFimDaLista()-1); // Decrementa em 1 unidade o atributo Fim da Lista
            }   
            
            return true; // Retorna dizendo que foi possível remover
            
        }
        
    }
    
    /* Método para alterar dados de um usuário da lista.
       
       A forma de alteração poderá ser:
       1 - Acessando direto a posição, ou
       2 - Pesquisando pela chave (código) e encontrando o usuário,
           alteram-se os dados. */
    
    public int alterarDados(int codigoAntigo, int novoCodigo, String novoNome){
        
        // Pesquisando o código na lista
        int p = this.pesquisarElemento(codigoAntigo);
        
        // Se retornar uma posição inválida
        // Retorna para JFramePrincipal - 1
        if (p == -1){
            
            return -1;
        
        }else{
            
            // Caso contrário, substitui os valores do objeto (presente na posição 'p')
            this.getLista()[p].setCodigoUsuario(novoCodigo);
            this.getLista()[p].setNomeUsuario(novoNome);
        
            return p; // 'p' servirá para localizar a posição exata no JTableDados
        
        }
        
        // Provavelmente a assinatura do método será alterada.
        
    }
    
    // Método para pesquisar um determinado registro através
    // de uma chave (na aplicação, essa chave será o código).
    public int pesquisarElemento(int codigo){
        
        int posicao;
        
        // Percorre a Lista começando na posição '0' até o fim da lista.
        for (posicao = 0; posicao <= this.getFimDaLista(); posicao++)
            if(codigo == this.getLista()[posicao].getCodigoUsuario()) // Encontrando o código na lista,
                return posicao;                           // retorna a posição.

        return -1; // Retorna -1 (posição inválida para Lista)
        
    }
    
    // Método para resgatar um elemento de uma determianda posição 'p'.
    //public Usuario acessarDados(int p){
    public String acessarDados(int p){
        int codigoUsuario;
        String nomeUsuario;
        
        // Se a Lista estiver vazia ou posição negativa ou maior que o fim da lista
        if ((this.isEmpty()) || (p < 0) || (p > this.getFimDaLista()))
            return null; // retorna nulo
        else
            codigoUsuario = this.getLista()[p].getCodigoUsuario(); // Retona o valor do objeto da posição 'p'.
            nomeUsuario = this.getLista()[p].getNomeUsuario(); // Retona o valor do objeto da posição 'p'.
            return "Código: " + codigoUsuario + ". Nome: " + nomeUsuario + "!";
            //return this.getLista()[p]; // Retona o objeto da posição 'p'.
    }
    
    // Método para listar os elementos da lista contígua.
    public String listarElementos() {
        String todosElementos = "";
        for (int index = 0; index < this.TamanhoDaLista; index++) {
            todosElementos += this.acessarDados(index) + "\n";
            //System.out.println(this.acessarDados(index));
        }
        return todosElementos;
    }
    
    // Método para dividir listas
    
    // Método para concatenar listas
    
    // Método para ordenar a lista contígua
    
    
}
