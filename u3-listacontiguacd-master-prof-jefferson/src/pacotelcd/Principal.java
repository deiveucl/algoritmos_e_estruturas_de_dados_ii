package pacotelcd;

import java.util.Scanner;

/**
 *
 * @author Jefferson
 */
class Principal {

    public static void main(String[] args) {

        Scanner entrada = new Scanner(System.in);
        
        //EstruturasDeDados Lista;
        
        //int posicao = 0;
        
        //Lista = new EstruturasDeDados(posicao);
        
        EstruturasDeDados Lista = new EstruturasDeDados(10);
        
        for (int i = 0; i < 10; i++) {
            Lista.inserirDados(i, i + 1, "a" + (i + 100) + "b");
        }
        
        //Lista.listarElementos();
        
        // APENAS UM TESTE ESSAS LINHAS ABAIXO
        String todos = "";
        todos += "abc" + "\n";
        todos += "abc" + "\n";
        System.out.println(todos);

    }
    
}
