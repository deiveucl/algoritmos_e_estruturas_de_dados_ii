package pacotelecd;

import javax.swing.JOptionPane;

/**
 *
 * @author Jefferson
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        /*-----------------------------------
                 ATRIBUTOS DA CLASSE
          -----------------------------------*/
        
        int opcao;  // Opção para trabalhar o menu principal.
        
        int cdu;    // Código do usuário.
        String ndu; // Nome do usuário.
        int pdunl;  // Posição do usuário na lista.
        
        // Atributo para manipular o descritor e as operaÃ§Ãµes da lista.
        EstruturasDeDados LE = new EstruturasDeDados();
        
        /*-----------------------------------
                   OPERAÇÕES do MENU
          -----------------------------------*/
        
        do{
            
            opcao = menuPrincipal();
            
            switch(opcao){
                
                case 0: LE.limparLE();
                        JOptionPane.showMessageDialog(null,"Saindo da aplicação...");
                        break;
                
                case 1: LE.inicializarLE();
                        JOptionPane.showMessageDialog(null, "LE com descritor criada com sucesso!");
                        break;
                        
                case 2:	cdu   = Integer.parseInt(JOptionPane.showInputDialog(null,"Código do usuário: "));
                        ndu   = JOptionPane.showInputDialog(null,"Nome do usuário: ");
                        
                        pdunl = Integer.parseInt(JOptionPane.showInputDialog(null,"Posição do usuário na lista: "));
                        
                        if (LE.inserirDados(cdu,ndu,pdunl))
                            JOptionPane.showMessageDialog(null,"Usuário " + ndu + " inserido!");
                        else
                            JOptionPane.showMessageDialog(null,"Usuário " + ndu + " não inserido ou já presente na lista!");
                                
                        break;

                case 3:	pdunl = Integer.parseInt(JOptionPane.showInputDialog(null,"Posição do usuário na lista: "));
                        
                        if (LE.removerDados(pdunl))
                            JOptionPane.showMessageDialog(null,"Usuário removido!");
                        else
                            JOptionPane.showMessageDialog(null,"Usuário não removido ou não está presente na lista!");
                        
                        break;

                case 4: cdu = Integer.parseInt(JOptionPane.showInputDialog(null,"Código do usuário a ser pesquisado: "));
                        
                        /*if(LE.pesquisarElemento(cdu) != -1)
                            JOptionPane.showMessageDialog(null,"Usuário " + cdu + " encontrado!");
                        else
                            JOptionPane.showMessageDialog(null,"Usuário " + cdu + " não encontrado ou não existe(m) candidato(s) na lista!");
                       */
                        break;

                case 5: cdu = Integer.parseInt(JOptionPane.showInputDialog(null,"Código do usuário a ser alterado: "));
                        
                        // ncdu -> novo código do usuário
                        // nndu -> novo nome do usuário
                        int ncdu    = Integer.parseInt(JOptionPane.showInputDialog(null,"Código do novo usuário: "));
                        String nndu = JOptionPane.showInputDialog(null,"Nome do novo usuário: ");
                        
                        /*if(LE.alterarDados(cdu,ncdu,nndu))
                            JOptionPane.showMessageDialog(null,"Dados alterados com sucesso!");
                        else
                            JOptionPane.showMessageDialog(null,"Erro ao alterar os dados ou candidato não existe(m) candidato(s) na lista!");
                        */
                        break;
                
                case 6: String relatorio = LE.imprimirLista();
                        
                        if( !"".equals(relatorio) )
                            JOptionPane.showMessageDialog(null,relatorio);
                        else
                            JOptionPane.showMessageDialog(null,"Não existe(m) candidato(s) inscrito(s)!");
                        
                        break;
                
                default: JOptionPane.showMessageDialog(null,"Opção inválida!","Advertência",JOptionPane.WARNING_MESSAGE);
                
            }
            
        }while(opcao != 0);

    }
    
    // MÉtodo para acessar o menu principal da aplicação.
    private static int menuPrincipal(){
        
        return Integer.parseInt(JOptionPane.showInputDialog(null, 
                                                                  "=======================       \n"
                                                                + " 1 - Criar lista              \n"
                                                                + " 2 - Inserir usuário          \n"
                                                                + " 3 - Excluir usuário          \n"
                                                                + " 4 - Pesquisar usuário        \n"
                                                                + " 5 - Alterar dados do usuário \n"
                                                                + " 6 - Relatório de dados       \n"
                                                                + " 0 - Sair da aplicação        \n"
                                                                + "=======================       \n"
                                                                + "    Digite uma opção válida   \n"
                                                                + "=======================       \n",
                                                                     
                                                                "Operações na LDE", JOptionPane.QUESTION_MESSAGE));
        
    }
    
}
