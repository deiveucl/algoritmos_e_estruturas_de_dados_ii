package pacotelecd;

/**
 *
 * @author Jefferson
 */
class Usuario {
    
    /*-----------------------------------
              ATRIBUTOS DA CLASSE
      -----------------------------------*/
    
    private int codigoUsuario;
    private String nomeUsuario;
    
    /*-----------------------------------
             CONSTRUTOR DA CLASSE
      -----------------------------------*/
    
    // _codU  = código do usuário
    // _nomeU = nome do usuário
    public Usuario(int _codU, String _nomeU) {
        this.codigoUsuario = _codU;
        this.nomeUsuario   = _nomeU;
    }
    
    /*-----------------------------------
               MÉTODOS DA CLASSE
      -----------------------------------*/
    
    // Método para resgatar o código do usuário
    public int getCodigoUsuario() {
        return this.codigoUsuario;
    }

    // Método para registrar o código do usuário
    // _codU  = código do usuário
    public void setCodigoUsuario(int _codU) {
        this.codigoUsuario = _codU;
    }

    // Método para resgatar o nome do usuário
    public String getNomeUsuario() {
        return this.nomeUsuario;
    }

    // Método para registrar o nome do usuário
    // _nomeU = nome do usuário
    public void setNomeUsuario(String _nomeU) {
        this.nomeUsuario = _nomeU;
    }
           
}
