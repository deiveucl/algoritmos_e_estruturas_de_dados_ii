package pacotelecd;

/**
 *
 * @author Jefferson
 */
class No {
    
    /*-----------------------------------
              ATRIBUTOS DA CLASSE
      -----------------------------------*/

    private Usuario objeto;
    private No referencia;

    /*-----------------------------------
             CONSTRUTOR DA CLASSE
      -----------------------------------*/
    
    // _c = código
    // _n = nome
    public No(int _c, String _n) {
        
        Usuario u = new Usuario(_c,_n);
        
        this.setObjeto(u);
        
    }
    
    /*-----------------------------------
               MÉTODOS DA CLASSE
      -----------------------------------*/

    // Método set para registrar a referência do próximo nó da lista.
    // _rPN = referência do próximo nó
    public void setProximo(No _rPN) {
        this.referencia = _rPN;
    } 

    // Método get para resgatar a referência do próximo nó da lista.
    public No getProximo() {
        return this.referencia;
    }

    // Método set para registrar a instância do usuário.
    // _u = usuário
    public void setObjeto(Usuario _u){
        this.objeto = _u;
    }
    
    // Método get para resgatar o objeto Usuaário.
    public Usuario getObjeto() {
        return this.objeto;
    }
    
}
