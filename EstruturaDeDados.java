/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package numerocomplexo;

/**
 *
 * @author Deive
 */
public class EstruturaDeDados {
    
    // CRIADA COM BASE NO CÓDIGO DEMONSTRADO EM SALA DE AULA
    
    // ATRIBUTOS DA CLASSE
    private NumeroComplexoTad[] ListaNumeroComplexo;
    private int fimLista;
    private int tamLista;
    
    // CONSTRUTOR DA CLASSE
    public EstruturaDeDados(int tamLista) {
        this.criarLista(tamLista);
    }
    
    // OPERAÇÕES DO TIPO ABSTRATO DE DADOS - LISTA
    // CRIANDO A LISTA CONTÍGUA
    private void criarLista(int tamLista) {
        this.setLista(new NumeroComplexoTad[tamLista]);
        
        this.setFimLista(-1);
        this.setTamLista(tamLista);
    }
    // LIMPANDO A LISTA CONTÍGUA
    private void limparListaContigua() {
        this.setFimLista(-1);
    }
    // VERIFICA SE A LISTA ESTÁ VAZIA OU NÃO
    private boolean estaVazia() {
        return this.tamLista == -1;
    }
    // VERIFICA SE A LISTA ESTÁ CHEIA
    private boolean estaCheia() {
        return this.tamLista - 1 == this.fimLista;
    }
    // INSERINDO DADOS NA LISTA
    public boolean inserirDados(int posicao, double num_real_a, double num_real_b) {
        // VERIFICA SE A LISTA ESTÁ CHEIA, OU SE A POSIÇÃO INDICADA É NEGATIVA,
        // OU SE A POSIÇÃO INDICADA É MAIOR QUE O FIM DA LISTA
        if (this.estaCheia() || posicao < 0 || posicao > this.fimLista) {
            return false;
        } else {
            // CRIANDO UMA NOVA INSTÂNCIA DO NÚMERO COMPLEXO
            NumeroComplexoTad numCom = new NumeroComplexoTad(num_real_a, num_real_b);
            
            // VERIFICA SE IRÁ INSERIR NA PRÓXIMA POSIÇÃO VÁLIDA
            if (posicao == this.getFimLista() + 1) {
                this.setFimLista(this.getFimLista() + 1);
                this.getListaNumeroComplexo()[posicao] = numCom;
            } else {
                // ABRE UM ESPAÇO NA LISTA PARA INSERIR O NOVO VALOR
                for (int i = this.getFimLista(); i >= posicao; i--) {
                    this.getListaNumeroComplexo()[i + 1] = this.getListaNumeroComplexo()[i];
                }
                
                // INSERE O NOVO VALOR NA POSIÇÃO ABERTA
                this.getListaNumeroComplexo()[posicao] = numCom;
                this.setFimLista(this.getFimLista() + 1);
            }
            
            return true;
        }
    }
    /*// REMOVER DADO
    public boolean deletarDados(int posicao) {
        // VERIFICA SE A LISTA ESTÁ CHEIA, OU SE A POSIÇÃO INDICADA É NEGATIVA,
        // OU SE A POSIÇÃO INDICADA É MAIOR QUE O FIM DA LISTA
        if (this.estaCheia() || posicao < 0 || posicao > this.fimLista) {
            return false;
        } else {
            
        }
    }*/
    
    // MÉTODOS DA CLASSE
    // SETANDO A LISTA DE NÚMEROS COMPLEXOS
    public void setLista(NumeroComplexoTad[] numCom) {
        this.ListaNumeroComplexo = numCom;
    }
    // ATRIBUINDO UM VALOR PARA O ATRIBUTO fimLista
    public void setFimLista(int fimLista) {
        this.fimLista = fimLista;
    }
    // RETORNANDO A POSIÇÃO DO ÚLTIMO ELEMENTO DA LISTA
    public int getFimLista() {
        return this.fimLista;
    }
    // ATRIBUINDO UM VALOR PARA O TAMANHO DA LISTA
    public void setTamLista(int tamLista) {
        this.tamLista = tamLista;
    }
    // RETORNANDO O TAMANHO DA LISTA
    public int getTamLista() {
        return this.tamLista;
    }
    // RETORNANDO A LISTA DE NÚMEROS COMPLEXOS
    public NumeroComplexoTad[] getListaNumeroComplexo() {
        return this.ListaNumeroComplexo;
    }
    
}
