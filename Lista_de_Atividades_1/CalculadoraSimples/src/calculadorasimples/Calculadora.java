/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadorasimples;

/**
 *
 * @author Deive
 */
public class Calculadora {
    
    private final double num_1;
    private final double num_2;
    private final char operacao;
    private String resultado;
    
    public Calculadora(double num_1, double num_2, char operacao) {
        this.num_1 = num_1;
        this.num_2 = num_2;
        this.operacao = operacao;
    }
    
    public void CalcularResultado() {
        switch (this.operacao) {
            case '+':
                resultado = Double.toString(num_1 + num_2);
                break;
            case '-':
                resultado = Double.toString(num_1 - num_2);
                break;
            case '*':
                resultado = Double.toString(num_1 * num_2);
                break;
            case '/':
                // Dividindo inteiro por zero, acontece o erro de Exception
                // DIvidindo double por zero, o Java entende que é um valor infinito..
                // pois quanto mais o divisor for próximo de zero, maior o resultado da divisão... (tende ao infinito)
                resultado = Double.toString(num_1 / num_2);
                if (resultado.equals("Infinity")) {
                    resultado = "Impossível dividir por zero!";
                }
                break;
            default:
                resultado = "Operação inválida";
                break;
        }
    }
    
    public void ImprimirResultado() {
        System.out.println(resultado);
    }
    
}
