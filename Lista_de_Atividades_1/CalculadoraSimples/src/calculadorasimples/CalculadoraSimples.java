/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadorasimples;

/**
 *
 * @author Deive
 */
public class CalculadoraSimples {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        /*
        Escreva uma classe em java que simule uma calculadora bem simples. Essa classe deve ter
        como atributos duas variáveis double e um char. Deve possuir um construtor que recebe
        como parâmetros dois números e um caractere, correspondente a uma das operações
        básicas (+,-,*,/). Deve ter um método para calcular a operação desejada e um para imprimir
        o resultado. O programa deve considerar divisões por zero como sendo erros, e imprimir
        uma mensagem adequada.
        */
        
        Calculadora soma = new Calculadora(10.1, 5.2, '+');
        soma.CalcularResultado();
        soma.ImprimirResultado();
        
        Calculadora subtracao = new Calculadora(10, 5.4, '-');
        subtracao.CalcularResultado();
        subtracao.ImprimirResultado();
        
        Calculadora multiplicacao = new Calculadora(10.3, 5, '*');
        multiplicacao.CalcularResultado();
        multiplicacao.ImprimirResultado();
        
        Calculadora divisao = new Calculadora(10, 2.5, '/');
        divisao.CalcularResultado();
        divisao.ImprimirResultado();
        
        Calculadora divisaoPorZero = new Calculadora(5, 0, '/');
        divisaoPorZero.CalcularResultado();
        divisaoPorZero.ImprimirResultado();
        
    }
    
}
