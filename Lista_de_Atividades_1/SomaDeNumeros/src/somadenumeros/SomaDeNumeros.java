/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package somadenumeros;

import java.util.Scanner;

/**
 *
 * @author Deive
 */
public class SomaDeNumeros {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*
        Faça um programa em java que leia vários números. Caso o usuário digite -1, o programa
        para de solicitar os números e imprime a soma de todos os números digitados, sem contar
        com o -1.
        Nome do programa: SomaDeNumeros.java
        */

        Scanner meuTeclado = new Scanner(System.in);

        double somatorio = 0, numeroEscolhido = 0;

        do {
            System.out.print("Informe um número qualquer: ");
            numeroEscolhido = meuTeclado.nextDouble();
            somatorio += (numeroEscolhido == -1) ? 0 : numeroEscolhido;
        } while (numeroEscolhido != (-1));

        meuTeclado.close();

        System.out.println("O somatório é igual a " + somatorio);
    }
    
}
