/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estadoslampadas;

/**
 *
 * @author Deive
 */
public class Lampada {
    
    private String estado;

    public Lampada() {
        this.estado = "Apagada";
    }
    
    public void Acender() {
        this.estado = "Acessa";
    }
    
    public void Apagar() {
        this.estado = "Apagada";
    }
    
    public void AcenderMeiaLuz() {
        this.estado = "Meia-luz";
    }
    
    public String GetEstado() {
        return this.estado;
    }
    
}
