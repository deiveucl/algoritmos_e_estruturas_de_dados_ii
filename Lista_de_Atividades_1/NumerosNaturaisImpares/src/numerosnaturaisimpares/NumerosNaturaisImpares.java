/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package numerosnaturaisimpares;
import java.util.Scanner;

/**
 *
 * @author Deive
 */
public class NumerosNaturaisImpares {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        /*
        Dado um número inteiro positivo n, crie um programa em java para imprimir os n
        primeiros naturais ímpares. Exemplo: para n=4 a saída deverá ser 1,3,5,7.
        Nome do programa: NumerosNaturaisImpares.java
        */

        Scanner meuTeclado = new Scanner(System.in);

        System.out.print("A partir de \"1\", quantos números naturais ímpares você deseja visualizar? ");
        int qtdNumerosNaturaisImpares = meuTeclado.nextInt();

        int numeroNaturalImparAtual = 1;

        for (int i = 0; i < qtdNumerosNaturaisImpares; i++) {
            if (i == qtdNumerosNaturaisImpares - 1) {
                System.out.println(numeroNaturalImparAtual + ".");
            } else {
                System.out.print(numeroNaturalImparAtual + ", ");

            }
            numeroNaturalImparAtual += 2;
        }

        meuTeclado.close();
        
    }
    
}
