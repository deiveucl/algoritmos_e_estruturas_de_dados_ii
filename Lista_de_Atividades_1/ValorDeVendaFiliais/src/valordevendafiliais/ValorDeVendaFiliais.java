/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package valordevendafiliais;

import java.io.FileInputStream;
import java.util.Scanner;
import java.io.IOException;

/**
 *
 * @author Deive
 */
public class ValorDeVendaFiliais {

    public static void main(String[] args) {
        
        /*
        Uma loja possui 4 filiais, cada uma com um código de 1 a 4.
        Um arquivo contendo todas as vendas feitas durante o dia nas quatro filiais é gerado a
        partir de uma planilha, sendo que cada linha do arquivo contém o número da filial e o valor
        de uma venda efetuada, separados por vírgula.
        Ex.:
        1,189.90
        1,45.70
        3,29.90
        4,55.00

        No exemplo acima, a filial 1 fez duas vendas, a primeira
        totalizando R$ 189,90 e a segunda R$ 45,70. A filial 3 fez
        uma venda de R$ 29,90 e a 4 também uma de R$ 55,00.
        Faça um programa que leia este arquivo e informe, ao
        final, o total e o valor médio das vendas de cada filial.
        */
        
        // PARA FAZER ESSE EXERCÍCIO DINAMICAMENTE (SEM USAR 70 VARIÁVEIS)
        // CONSIDEREI QUE AS FILIAIS ESTAVAM ORDENADAS
        
        FileInputStream fis = null;
        Scanner sc = null;
        
        try {
            fis = new FileInputStream("src\\valordevendafiliais\\vendas.txt");
            sc = new Scanner(fis);
        } catch (IOException e) {
            System.out.println("Um erro ocorreu:");
            System.out.println(e);
        }
        
        try {
            String linhaCompleta = sc.nextLine();
            String filialAtual = linhaCompleta.substring(0, linhaCompleta.indexOf(","));
            String filialAnterior = filialAtual;
            
            double vendaTotal = Double.parseDouble(linhaCompleta.substring(linhaCompleta.indexOf(",") + 1));
            double qtdFilial = 1;
            
            do {
                linhaCompleta = sc.nextLine();
                filialAtual = linhaCompleta.substring(0, linhaCompleta.indexOf(","));
                
                if (filialAtual.equals(filialAnterior)) {
                    vendaTotal += Double.parseDouble(linhaCompleta.substring(linhaCompleta.indexOf(",") + 1));
                    qtdFilial++;
                } else {
                    System.out.println("A filial " + filialAnterior + " vendeu um total de R$ " + String.format("%.2f", vendaTotal) + " com uma média de R$" + String.format("%.2f", (vendaTotal / qtdFilial)));
                    filialAnterior = filialAtual;
                    vendaTotal = Double.parseDouble(linhaCompleta.substring(linhaCompleta.indexOf(",") + 1));
                    qtdFilial = 1;
                }
                
                if (!sc.hasNextLine()) {
                    System.out.println("A filial " + filialAtual + " vendeu um total de R$ " + String.format("%.2f", vendaTotal) + " com ume média de R$ " + String.format("%.2f", (vendaTotal / qtdFilial)));
                }
            } while (sc.hasNextLine());
        } catch (NullPointerException e) {
            System.out.println("Chegou ao final do arquivo");
            System.out.println(e);
        }
        
    }
    
}