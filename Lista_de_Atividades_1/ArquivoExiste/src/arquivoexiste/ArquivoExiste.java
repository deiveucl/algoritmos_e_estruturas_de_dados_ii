/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arquivoexiste;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.io.FileWriter;
import java.io.PrintWriter;

/**
 *
 * @author Deive
 */
public class ArquivoExiste {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        /*
        Escreva um programa em java que receba um nome de arquivo e uma sequência de
        palavras como argumentos na linha de comando, informe se o arquivo existe ou não, caso
        não exista, crie-o e, por fim, escreva neste arquivo a sequência de palavras passadas como
        argumentos.
        */
        
        Scanner teclado = new Scanner(System.in);
        
        System.out.print("Escreva o nome do arquivo: ");
        String nomeArquivo = teclado.nextLine();
        
        System.out.print("Escreva a sequência de palavras, usando ; como separador: ");
        Scanner palavras = new Scanner(teclado.nextLine());
        
        teclado.close();
        
        palavras.useDelimiter(";");
        
        File f = new File("src/arquivoexiste/" + nomeArquivo);
        if (f.exists()) {
            System.out.println("O arquivo existe...");
            System.out.println("Nada a fazer...");
        } else {
            System.out.println("O arquivo não existe...");
            System.out.println("Criando-o...");
            
            try {
                f.createNewFile();
            } catch (IOException e) {
                System.out.println("O arquivo não pôde ser criado...");
                System.out.println(e);
            }
        }
        
        FileWriter fw = null;
        PrintWriter pw = null;
        
        try {
            fw = new FileWriter(f, true);
            pw = new PrintWriter(fw);
            
            while (palavras.hasNext()) {
                pw.println(palavras.next());
            }
            palavras.close();
            
            pw.close();
            fw.close();
            
            System.out.println("Palavras armazenadas com sucesso no arquivo");
        } catch (IOException e) {
            System.out.println("Falha ao gravar no arquivo...");
            System.out.println(e);
        }
        
    }
    
}
