/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vetorordenado;

import java.util.Scanner;
import java.util.Arrays;

/**
 *
 * @author Deive
 */
public class VetorOrdenado {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        
        /*
        Faça um programa em java que leia 10 valores inteiros, armazene-os em um vetor. Crie um
        outro vetor que terá os números inteiros armazenados em ordem crescente. Após a
        ordenação, apresente o conteúdo dos dois vetores.
        Nome do programa: VetorOrdenado.java
        */
        
        Scanner leitor = new Scanner(System.in);
        
        int[] valoresInteiros = new int[10];
        
        for (int i = 0; i < 10; i++) {
            System.out.print("Insira um valor inteiro para a " + (i + 1) + "ª posição: ");
            valoresInteiros[i] = leitor.nextInt();
        }
        
        int[] valoresCopiados = valoresInteiros.clone();
        
        int temp;
        for (int i = 0; i < valoresCopiados.length; i++) {
            for (int j = 0; j < valoresCopiados.length; j++) {
                if (j < valoresCopiados.length - 1) {
                    if (valoresCopiados[j] > valoresCopiados[j + 1]) {
                        temp = valoresCopiados[j];
                        valoresCopiados[j] = valoresCopiados[j + 1];
                        valoresCopiados[j + 1] = temp;
                    }
                }
            }
        }
        
        //Arrays.sort(valoresCopiados);
        
        System.out.println("\nValores do primeiro vetor (valores inseridos)");
        for (int v : valoresInteiros) {
            System.out.print(v + ", ");
        }
        
        System.out.println("\n\nValores do segundo vetor (valores ordenados)");
        for (int v : valoresCopiados) {
            System.out.print(v + ", ");
        }
        
    }
    
}
