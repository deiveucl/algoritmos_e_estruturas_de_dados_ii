/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exerciciooito;

import java.util.Scanner;
import java.util.Arrays;

/**
 *
 * @author Deive
 */
public class ExercicioOito {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        /*
        Escreva um programa que leia uma sequência com n números reais e imprime a sequência
        eliminando os elementos repetidos.
        */
        
        Scanner leitor = new Scanner(System.in);
        
        System.out.print("Quantos números reais deseja inserir? ");
        int qtdNumerosReais = leitor.nextInt();
        
        double[] arrNumerosReais = new double[qtdNumerosReais];
        
        for (int i = 0; i < arrNumerosReais.length; i++) {
            System.out.print("Insira um número real para a " + (i + 1) + "ª posição: ");
            arrNumerosReais[i] = leitor.nextDouble();
        }
        
        // VERIFICANDO QUANTOS ELEMENTOS DUPLICADOS EXISTEM
        Arrays.sort(arrNumerosReais);
        
        int qtdDuplicados = 0;
        for (int i = 0; i < arrNumerosReais.length; i++) {
            if (i != (arrNumerosReais.length - 1)) {
                if (arrNumerosReais[i] == arrNumerosReais[i + 1]) {
                    qtdDuplicados++;
                }
            }
        }
        
        // NOVO VETOR SEM VALORES DUPLICADOS
        double[] arrNumerosReaisNoDup = new double[arrNumerosReais.length - qtdDuplicados];
        
        double sentinela = arrNumerosReais[0];
        arrNumerosReaisNoDup[0] = sentinela;
        int contadorSemDup = 1;
        for (int i = 1; i < arrNumerosReais.length; i++) {
            if (sentinela != arrNumerosReais[i]) {
                sentinela = arrNumerosReais[i];
                arrNumerosReaisNoDup[contadorSemDup] = sentinela;
                contadorSemDup++;
            }
        }
        
        System.out.println("\nOs valores (sem registros duplicados) são os seguintes:");
        for (double v : arrNumerosReaisNoDup) {
            System.out.print(v + " - ");
        }
        
    }
    
}
