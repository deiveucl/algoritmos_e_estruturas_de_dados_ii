/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritmosdebusca;

/**
 *
 * @author Deive
 */
public class AlgoritmosDeBusca {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        // CRIANDO E PREENCHENDO UM VETOR
        int[] meuVetor = new int[] {1, 2, 3, 4, 3, 2, 5, 6, 7, 8};
        
        System.out.println("1ª posição tem valor: " + meuVetor[0] + "\n10ª posição tem valor: " + meuVetor[meuVetor.length - 1]);
        
        // ALGORITMO DE BUSCA SEQUENCIAL
        // PESQUISANDO POR REG E SUBSTITUINDO O VALOR
        // DECLARANDO MEU REG PARA PESQUISA DOS DADOS
        int reg = 99;
        
        System.out.println("\nLista não ordenada - Vetor não ordenado");
        
        int i = 0;
        int chaveBusca = 3;
        while (i < meuVetor.length) {
            if (meuVetor[i] == chaveBusca) {
                System.out.println("Valor " + meuVetor[i] + " na posição " + i + " substituido por " + reg);
                meuVetor[i] = reg;
            }
            i++;
        }
        
        // VETOR PRECISA ESTAR ORDENADO PARA PESQUISA BINÁRIA
        // ORDENANDO O VETOR UTILIZANDO MÉTODO BOLHA
        int m = meuVetor.length - 1;
        for (int k = 0; k < meuVetor.length; k++) {
            for (int j = 0; j < m; j++) {
                if (meuVetor[j] > meuVetor[j + 1]) {
                    int temp = meuVetor[j];
                    meuVetor[j] = meuVetor[j + 1];
                    meuVetor[j + 1] = temp;
                }
            }
            
            m--;
        }
        
        System.out.println("\nLista ordenada - Pesquisa Binária");
        
        // FAZENDO A PESQUISA BINÁRIA
        i = 0;
        chaveBusca = 7;
        int ultimaPos = meuVetor.length - 1;
        int metade = (i + ultimaPos) / 2;
        
        while (i <= ultimaPos) {
            if (meuVetor[metade] == chaveBusca) {
                System.out.println("Valor " + meuVetor[metade] + " na posição " + metade + " substituido por " + reg);
                meuVetor[metade] = reg;
            } else {
                if (chaveBusca < meuVetor[metade]) {
                    ultimaPos = metade - 1;
                } else {
                    i = metade + 1;
                }
            }
            
            metade = (i + ultimaPos) / 2;
        }
        
    }
    
}
