/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package somamatrizes;

/**
 *
 * @author Deive
 */
public class SomaMatrizes {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        /*
        
        */
        
        double[][] matriz_1 = new double[][] {{1, 2, 3, 4},
                                              {5, 6, 7, 8}};
        
        double[][] matriz_2 = new double[][] {{9, 10, 11, 12},
                                              {13, 14, 15, 16}};
        
        double[][] matriz_3 = new double[2][4];
        
        for (int linha = 0; linha < matriz_1.length; linha++) {
            for (int coluna = 0; coluna < matriz_1[0].length; coluna++) {
                matriz_3[linha][coluna] = matriz_1[linha][coluna] + matriz_2[linha][coluna];
            }
        }
        
        for (int linha = 0; linha < matriz_1.length; linha++) {
            for (int coluna = 0; coluna < matriz_1[0].length; coluna++) {
                System.out.println("" + matriz_1[linha][coluna] + " + " + matriz_2[linha][coluna] + " = " + matriz_3[linha][coluna]);
            }
        }
        
    }
    
}
